<!DOCTYPE html>
<html>
	<head>
		<title>Metabolic network modelling</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="mystyle.css">
	</head>

	<body>
		<h1>Metabolic network modelling</h1>
		<img src="kegg_map.png" alt="KEGG map" height="225" width="367"/>
		
		<p><b>Metabolic network reconstruction</b> and simulation allows for an in-depth insight into the molecular mechanisms of a particular organism. In particular, these models correlate the genome with molecular physiology.<sup>[1]</sup> A reconstruction breaks down metabolic pathways (such as glycolysis and the Citric acid cycle) into their respective reactions and enzymes, and analyzes them within the perspective of the entire network. In simplified terms, a reconstruction collects all of the relevant metabolic information of an organism and compiles it in a mathematical model. Validation and analysis of reconstructions can allow identification of key features of metabolism such as growth yield, resource distribution, network robustness, and gene essentiality. This knowledge can then be applied to create novel biotechnology.</p>
		<p>In general, the process to build a reconstruction is as follows:</p>

		<ol>
		<li>Draft a reconstruction</li>
		<li>Refine the model</li>
		<li>Convert model into a mathematical/computational representation</li>
		<li>Evaluate and debug model through experimentation</li>
		</ol>

		<h2>Genome-Scale Metabolic Reconstruction</h2>

		<p>A metabolic reconstruction provides a highly mathematical, structured platform on which to understand the systems biology of metabolic pathways within an organism.<sup>[2]</sup> The integration of biochemical metabolic pathways with rapidly available, unannotated genome sequences has developed what are called genome-scale metabolic models. Simply put, these models correspond metabolic genes with metabolic pathways. In general, the more information about physiology, biochemistry and genetics is available for the target organism, the better the predictive capacity of the reconstructed models. Mechanically speaking, the process of reconstructing prokaryotic and eukaryotic metabolic networks is essentially the same. Having said this, eukaryote reconstructions are typically more challenging because of the size of genomes, coverage of knowledge, and the multitude of cellular compartments.<sup>[2]</sup> The first genome-scale metabolic model was generated in 1995 for Haemophilus influenzae.<sup>[3]</sup> The first multicellular organism, C. elegans, was reconstructed in 1998.<sup>[4]</sup> Since then, many reconstructions have been formed. For a list of reconstructions that have been converted into a model and experimentally validated, see:<a href="http://sbrg.ucsd.edu/InSilicoOrganisms/OtherOrganisms">sbrg.ucsd.edu/InSilicoOrganisms/OtherOrganisms</a>.</p>

		<h4>References:</h4>

		<ol>
		<li>Franke; Siezen, Teusink (2005). "Reconstructing the metabolic network of a bacterium from its genome.". Trends in Microbiology 13 (11): 550–558. doi:10.1016/j.tim.2005.09.001. PMID 16169729.</li>
		<li>Thiele, Ines; Bernhard Ø Palsson (January 2010). "A protocol for generating a high-quality genome-scale metabolic reconstruction". Nature Protocols 5 (1): 93–121. doi:10.1038/nprot.2009.203. PMC 3125167. PMID 20057383.</li>
		<li>Fleischmann, R. D.; Adams, M. D.; White, O; Clayton, R. A.; Kirkness, E. F.; Kerlavage, A. R.; Bult, C. J.; Tomb, J. F.; Dougherty, B. A.; Merrick, J. M. (1995). "Whole-genome random sequencing and assembly of Haemophilus influenzae Rd". Science 269 (1995): 496–512. doi:10.1126/science.7542800. PMID 7542800.</li>
		<li>The C. elegans Sequencing Consortium (1998). "Genome Sequence of the Nematode C. elegans: A Platform for Investigating Biology". Science 282 (5396): 2012–2018. doi:10.1126/science.282.5396.2012. PMID 9851916.</li>
		</ol>
		
		<!-- JavaScripti osa on kopeeritud Krista Rüütli GitHubi repositooriumist. Ise ei jõudnud õigel ajal jälgida -->
		<script>
		function myFunction() {
		    alert('Javascript töötab!');
		}
		</script>
		<p>Leht on olnud avatud:</p>
		<label id="minutes">00</label>:<label id="seconds">00</label>
		    <script type="text/javascript">
			var minutesLabel = document.getElementById("minutes");
			var secondsLabel = document.getElementById("seconds");
			var totalSeconds = 0;
			setInterval(setTime, 1000);
			function setTime()
			{
			    ++totalSeconds;
			    secondsLabel.innerHTML = pad(totalSeconds%60);
			    minutesLabel.innerHTML = pad(parseInt(totalSeconds/60));
			}
			function pad(val)
			{
			    var valString = val + "";
			    if(valString.length < 2)
			    {
				return "0" + valString;
			    }
			    else
			    {
				return valString;
			    }
			}
		    </script>
		<p>Praktikumi lõpuni on jäänud:</p>
		<script language="JavaScript">
		TargetDate = "02/14/2016 00:30 PM";
		BackColor = "palegreen";
		ForeColor = "navy";
		CountActive = true;
		CountStepper = -1;
		LeadingZero = true;
		DisplayFormat = "%%D%% Days, %%H%% Hours, %%M%% Minutes, %%S%% Seconds.";
		FinishMessage = "Tund on läbi!";
		</script>

		<?php 
				// Siia saab kirjutada PHP koodi.
				// Teeme counteri siia
				include("counter.php")
						
		?>
		
	</body>
